
******** 2. PATIENTS AVEC CONSO DE MATERIEL PR ESCARRE (Matelas, surmatelas, coussin) ***********;

/* Codes LPP pour lit médicalisé et/ou matelas ou surmatelas et/ou coussin anti-escarre*/
data LPP_MATELAS_ET_SURMATELAS;
set Matelas SurMatelas;
run;

data MaterielEscarres; 
set lpp_coussins_anti_escarre LPP_MATELAS_ET_SURMATELAS lpp_matelas_surmatelas lpp_lits_med;
run; 
proc sql;
create table MaterielEscarres as 
select distinct *
from MaterielEscarres
;
quit;*217;

*****on rcupère le matériel pour escarre****;
/* Dates de soin : entre début obs - 3 mois et fin obs + 3 mois */
proc sql;
create table ListeBENEF_DCIR_3_temp as 
select distinct a.ben_nir_idt, a.NUM_ENQ, a.BEN_RNG_GEM, tip_prs_ide
from PATIENTS_TIP_TOTAL as a 
     inner join MaterielEscarres as b on (a.TIP_PRS_IDE = b.CodeLPP),
	 pat_plaies_init_2012 as pat_plaies
where pat_plaies.ben_nir_idt = a.ben_nir_idt 
and a.EXE_SOI_DTD between MDY(10,1,2016) and MDY(3,31,2018)
;
quit; *353 163;


/*Repérage coussins */

proc sort data= ListeBENEF_DCIR_3_temp ; by tip_prs_ide ; run;
proc sort data= lpp_coussins_anti_escarre ; by CodeLPP ; run;

data ListeBENEF_DCIR_3_temp ;
merge ListeBENEF_DCIR_3_temp (in=a) lpp_coussins_anti_escarre (in=b rename= CodeLPP=tip_prs_ide) ;
by tip_prs_ide;
if a;
if b then coussins=1;
run;
*353 163;

proc sort data= ListeBENEF_DCIR_3_temp ; by ben_nir_idt ; run;


* a. En commun diabetiques/non diabetiques : matelas/surmatelas ;
data ListeBENEF_DCIR_3a ;
set ListeBENEF_DCIR_3_temp ; 
if coussins ne 1 ;
run ; *337 888;

* b. Uniquement pr les non diabetiques : coussin ;
data ListeBENEF_DCIR_3b ;
merge ListeBENEF_DCIR_3_temp (in=ina) ListeBENEF_DCIR_2 (in=inb); 
by ben_nir_idt ;
if ina and not inb and coussins = 1 ;
run ; *11 859;

* CONCAT ; 
data ListeBENEF_DCIR_3;
set ListeBENEF_DCIR_3a  ListeBENEF_DCIR_3b ; 
run ; *349 747;

proc sort data =  ListeBENEF_DCIR_3 out= ListeBENEF_DCIR_3_pat (keep=ben_nir_idt) nodupkey ; by ben_nir_idt ; run ; *142 267 ;





